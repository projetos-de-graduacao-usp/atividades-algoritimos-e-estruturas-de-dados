/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *  ____                              _        _   _           
 * / ___|  ___  _ __ ___   __ _    __| | ___  | \ | | ___  ___ 
 * \___ \ / _ \| '_ ` _ \ / _` |  / _` |/ _ \ |  \| |/ _ \/ __|
 *  ___) | (_) | | | | | | (_| | | (_| |  __/ | |\  | (_) \__ \
 * |____/ \___/|_| |_| |_|\__,_|  \__,_|\___| |_| \_|\___/|___/
 *                                                             
 *  _____ _ _ _               
 * |  ___(_) | |__   ___  ___ 
 * | |_  | | | '_ \ / _ \/ __|
 * |  _| | | | | | | (_) \__ \
 * |_|   |_|_|_| |_|\___/|___/
 *                           
 */

#include "binary_tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main()
{
	int node_tot;
	scanf(" %d", &node_tot);

	Tree *tree = create_tree();

	for (int i = 0; i < node_tot; i++) {
		int id;
		int value;
		int id_esq;
		int id_dir;

		scanf(" %d %d %d %d", &id, &value, &id_esq, &id_dir);

		insert_leaf(tree, id, value, id_esq, id_dir);
	}

	int result = eql_sum(get_root(tree));

	if (result == -1) {
		printf("FALSO\n");
	} else {
		printf("VERDADEIRO\n");
	}

	destroy(get_root(tree));

	free(tree);

	return 0;
}
