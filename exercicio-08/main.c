/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *  ____       _ _                       _           
 * |  _ \ ___ | (_)_ __   ___  _ __ ___ (_) ___  ___ 
 * | |_) / _ \| | | '_ \ / _ \| '_ ` _ \| |/ _ \/ __|
 * |  __/ (_) | | | | | | (_) | | | | | | | (_) \__	\
 * |_|   \___/|_|_|_| |_|\___/|_| |_| |_|_|\___/|___/
 *                                                   
 *  _____                                    
 * | ____|___ _ __   __ _ _ __ ___  ___  ___ 
 * |  _| / __| '_ \ / _` | '__/ __|/ _ \/ __|
 * | |___\__ \ |_) | (_| | |  \__ \ (_) \__	\
 * |_____|___/ .__/ \__,_|_|  |___/\___/|___/
 *           |_|                             
 *
 */

#include "linked_list.h"
#include <stdio.h>


int main()
{
	int test_tot;
	scanf(" %d", &test_tot);

	for (int i = 0; i < test_tot; i++) {
		int polinomios_tot;
		scanf(" %d", &polinomios_tot);

		Polinomio *polinomio_list[polinomios_tot];

		for (int j = 0; j < polinomios_tot; j++) {
			Polinomio *new_polinomio = create_polinomio();

			char stop;
			scanf(" %c", &stop);

			while (stop != ')') {
				Polinomio *new_coeff = register_coeff();
				insert(new_polinomio, new_coeff);

				scanf(" %c", &stop);
			}

			polinomio_list[j] = new_polinomio;
		}

		Polinomio *sum_polinomio = sum(polinomio_list, polinomios_tot);

		print_polinomio(sum_polinomio);
		putchar('\n');

		destroy(sum_polinomio);
		for (int j = 0; j < polinomios_tot; j++) {
			destroy(polinomio_list[j]);
		}
	}

	return 0;
}
