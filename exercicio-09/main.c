/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *   ____                     _           
 *  / ___|_ __ __ _ _ __   __| | ___  ___ 
 * | |  _| '__/ _` | '_ \ / _` |/ _ \/ __|
 * | |_| | | | (_| | | | | (_| |  __/\__ \
 *  \____|_|  \__,_|_| |_|\__,_|\___||___/
 *                                        
 *  _   _                                    
 * | \ | |_   _ _ __ ___   ___ _ __ ___  ___ 
 * |  \| | | | | '_ ` _ \ / _ \ '__/ _ \/ __|
 * | |\  | |_| | | | | | |  __/ | | (_) \__	\
 * |_| \_|\__,_|_| |_| |_|\___|_|  \___/|___/
 *                                           
 */

#include "bignum.h"
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


int main()
{
	int counter;
	scanf(" %d", &counter);

	for (int i = 0; i < counter; i++) {
		char *command = malloc(4 * sizeof(char));
		scanf(" %s", command);
		getchar();

		Big *number_1 = read_big();
		Big *number_2 = read_big();

		if (strcasecmp(command, "sum") == 0) {
			Big *number_sum = sum(number_1, number_2);

			print_big(number_sum);
			putchar('\n');
			
			destroy(number_sum);
			
		} else if (strcasecmp(command, "big") == 0) {
			int is_bigger = bigger(number_1, number_2);

			printf("%d\n", is_bigger);
			
		} else if (strcasecmp(command, "sml") == 0) {
			int is_smaller = smaller(number_1, number_2);

			printf("%d\n", is_smaller);
			
		} else if (strcasecmp(command, "eql") == 0) {
			int is_equal = equal(number_1, number_2);

			printf("%d\n", is_equal);
		}

		destroy(number_1);
		destroy(number_2);
		free(command);
	}

	return 0;
}
