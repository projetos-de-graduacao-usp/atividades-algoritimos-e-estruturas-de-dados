#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
	int value;
	void *next;
	void *prev;
} Node;

struct linked_list {
	Node **vec;
	Node *begin;
	Node *end;
	int size;
};


LinkedList *create()
{
	LinkedList *list = malloc(sizeof(LinkedList));

	list->vec = NULL;
	list->begin = NULL;
	list->size = 0;

	return list;
}


void insert(LinkedList *list, int value)
{
	Node *elem = malloc(sizeof(Node));

	elem->value = value;
	elem->next = NULL;
	elem->prev = NULL;

	int index = list->size;
	list->size++;

	list->vec = realloc(list->vec, (list->size + 1) * sizeof(Node *));
	list->vec[index] = elem;

	if (list->begin == NULL) {
		list->begin = elem;
		list->end = elem;
	} else {
		list->end->next = elem;
		elem->prev = list->end;
		list->end = elem;
	}
}


int get(LinkedList *list)
{
	assert(!is_empty(list));

	int value = list->begin->value;

	list->begin = list->begin->next;

	return value;
}


bool is_empty(LinkedList *list)
{
	return list->begin == NULL;
}


void rotate(LinkedList *list)
{
	list->end->next = list->begin;
	list->begin = list->end;
	list->end = list->end->prev;
}


void destroy(LinkedList *list)
{
	for (int i = 0; i < list->size; i++) {
		free(list->vec[i]);
	}

	free(list->vec);

	free(list);
}
