#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stdbool.h>

typedef struct linked_list LinkedList;

LinkedList *create(void);

void insert(LinkedList *list, int value);
// adiciona ao final da lista.

int get(LinkedList *list);
// remove o primeiro item da lista e retorna ele.

bool is_empty(LinkedList *list);

void rotate(LinkedList *list);

void destroy(LinkedList *list);


#endif /* LINKED_LIST_H */
