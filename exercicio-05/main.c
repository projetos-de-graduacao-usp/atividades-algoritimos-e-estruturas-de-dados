/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *  ____                                   _       
 * / ___|  ___  __ _ _   _  ___ _ __   ___(_) __ _ 
 * \___ \ / _ \/ _` | | | |/ _ \ '_ \ / __| |/ _` |
 *  ___) |  __/ (_| | |_| |  __/ | | | (__| | (_| |
 * |____/ \___|\__, |\__,_|\___|_| |_|\___|_|\__,_|
 *                |_|                              
 *  ____        _                                _       
 * | __ )  __ _| | __ _ _ __   ___ ___  __ _  __| | __ _ 
 * |  _ \ / _` | |/ _` | '_ \ / __/ _ \/ _` |/ _` |/ _` |
 * | |_) | (_| | | (_| | | | | (_|  __/ (_| | (_| | (_| |
 * |____/ \__,_|_|\__,_|_| |_|\___\___|\__,_|\__,_|\__,_|
 */

#include "check_pairs.h"
#include "string_plus.h"
#include <stdio.h>
#include <stdlib.h>


int main()
{
	bool first_run = true;

	do {
		char *my_string = read_line(stdin);

		if (!first_run)
			putchar('\n');
		else
			first_run = false;

		if (is_balanced(my_string)) {
			printf("BALANCEADO");
		} else {
			printf("NÃO BALANCEADO");
		}

		free(my_string);
	} while (!feof(stdin));

	return 0;
}
