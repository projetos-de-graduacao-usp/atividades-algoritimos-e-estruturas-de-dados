#include "check_pairs.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct stack {
	char *stack;
	uint16_t top;
};


bool is_balanced(char *my_string)
{
	uint16_t size = strlen(my_string);
	Stack *open_pairs = create(200);

	for (int i = 0; i < size; i++) {
		char pair = my_string[i];
		char open = ' ';

		if (!is_empty(open_pairs))
			open = top(open_pairs);

		if (pair == '{' || pair == '[' || pair == '(') {
			push(open_pairs, pair);
			continue;

		} else if (pair == '"' && open == '"') {
			// aspas precisam ser tratadas à parte já que não tem como
			// diferenciar onde elas estão sendo abertas ou fechadas.
			pop(open_pairs);
			continue;

		} else if (pair == '"') {
			push(open_pairs, pair);
			continue;

		} else if (pair == ' ') {
			continue;
		}


		if ((pair == '}' && open == '{') || (pair == ']' && open == '[') ||
			(pair == ')' && open == '(')) {
			pop(open_pairs);

		} else {
			empty(open_pairs);
			return false;
		}
	}


	if (is_empty(open_pairs)) {
		empty(open_pairs);
		return true;

	} else {
		// se a stack foi completamente percorrida sem retornar false e não
		// está vazia é porque sobrou alguma aspas não fechada.
		empty(open_pairs);
		return false;
	}
}


bool is_empty(Stack *my_stack)
{
	return my_stack->top == 0;
}


void empty(Stack *my_stack)
{
	free(my_stack->stack);
	free(my_stack);
}


char pop(Stack *my_stack)
{
	my_stack->top--;

	return my_stack->stack[my_stack->top];
}


Stack *create(int size)
{
	Stack *my_stack = malloc(sizeof(Stack));

	my_stack->stack = malloc(size * sizeof(char));
	my_stack->top = 0;

	return my_stack;
}


char top(Stack *my_stack)
{
	return my_stack->stack[my_stack->top - 1];
}


void push(Stack *my_stack, char value)
{
	my_stack->stack[my_stack->top] = value;
	my_stack->top++;
}
